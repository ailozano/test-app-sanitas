import { Component, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-item',
  template: ''
})
export class ItemMockComponent {

  @Input() set img(value: string) {
    this.el.nativeElement.setAttribute('mock-img', value);
  }

  @Input() set text(value: string) {
    this.el.nativeElement.setAttribute('mock-text', value);
  }

  constructor(
    private el: ElementRef
  ) { }

}
