import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list-item',
  template: ''
})
export class ListItemMockComponent {

  @Input() list;

}
