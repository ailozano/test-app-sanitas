import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { SearchBoxComponent } from './search-box.component';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBoxComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('emit search', () => {
    component.textChange = { emit: jest.fn() } as any;
    const e: Event = document.createEvent('Event');
    e.initEvent('input', false, false);

    const input = fixture.debugElement.query(By.css('input'));
    input.nativeElement.value = 'text-search';
    input.nativeElement.dispatchEvent(e);

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.textChange.emit).toHaveBeenCalledWith('text-searchdsa');
    });
  });
});
