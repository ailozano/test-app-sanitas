import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {

  @Output() textChange: EventEmitter<string> = new EventEmitter();

  search: FormControl;

  ngOnInit() {
    this.search = new FormControl();

    this.search.valueChanges.pipe(
      debounceTime(300)
    ).subscribe(value => {
      this.textChange.emit(value);
    });
  }

}
