import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('Text and image', () => {
    component.text = 'text';
    component.img = 'image';
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });

});
