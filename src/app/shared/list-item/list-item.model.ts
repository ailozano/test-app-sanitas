export interface ListItem {
  photo: string;
  text: string;
  id: string;
}
