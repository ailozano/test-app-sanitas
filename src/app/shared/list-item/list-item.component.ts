import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { Component, Input, ViewChild } from '@angular/core';
import { ListItem } from './list-item.model';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  listItems: ListItem[];

  @ViewChild(CdkVirtualScrollViewport, {static: true}) virtualScroll: CdkVirtualScrollViewport;

  @Input() set list(list: ListItem[]) {
    if (this.virtualScroll)  {
      this.virtualScroll.scrollToIndex(0);
    }

    this.listItems = list;
  }

}
