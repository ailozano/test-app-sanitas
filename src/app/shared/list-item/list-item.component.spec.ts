import { ScrollingModule } from '@angular/cdk/scrolling';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ItemMockComponent } from 'src/test/__mocks__/item';

import { ListItemComponent } from './list-item.component';

describe('ListItemComponent', () => {
  let component: ListItemComponent;
  let fixture: ComponentFixture<ListItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ListItemComponent, ItemMockComponent ],
      imports: [ ScrollingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListItemComponent);
    component = fixture.componentInstance;
  });

  it('Render list', fakeAsync(() => {

    component.virtualScroll = null;

    fixture.autoDetectChanges();
    tick(500);

    component.list = [
      {
        photo: 'photo 1',
        text: 'text 1',
        id: '1'
      },
      {
        photo: 'photo 2',
        text: 'text 2',
        id: '2'
      },
      {
        photo: 'photo 3',
        text: 'text 3',
        id: '3'
      }
    ];

    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  }));

});
