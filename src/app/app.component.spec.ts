import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListItemMockComponent } from 'src/test/__mocks__/list-item';
import { SearchBoxMockComponent } from 'src/test/__mocks__/search-box';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ListItemMockComponent,
        SearchBoxMockComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('populateList Fn', () => {
    expect(
      (component as any).populateList(3)
    ).toEqual([
      {
        photo: `https://picsum.photos/id/0/500/500`,
        text: 'Random text photo number 0',
        id: '0'
      },
      {
        photo: `https://picsum.photos/id/1/500/500`,
        text: 'Random text photo number 1',
        id: '1'
      },
      {
        photo: `https://picsum.photos/id/2/500/500`,
        text: 'Random text photo number 2',
        id: '2'
      }
    ]);
  });

  it('updateList Fn', () => {
    component.list = [
      {
        photo: `https://picsum.photos/id/0/500/500`,
        text: 'Random text photo number 0',
        id: '0'
      },
      {
        photo: `https://picsum.photos/id/1/500/500`,
        text: 'Random text photo number 1',
        id: '100'
      },
      {
        photo: `https://picsum.photos/id/2/500/500`,
        text: 'with 100',
        id: '2'
      }
    ];
    component.listSubject = { next: jest.fn() } as any;
    component.updateList('100'); // filter

    expect(component.listSubject.next).toHaveBeenCalledWith([
      {
        photo: `https://picsum.photos/id/1/500/500`,
        text: 'Random text photo number 1',
        id: '100' // has id
      },
      {
        photo: `https://picsum.photos/id/2/500/500`,
        text: 'with 100', // includes text
        id: '2'
      }
    ]);
  });

});
