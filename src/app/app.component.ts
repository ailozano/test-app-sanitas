import { Component, OnInit } from '@angular/core';
import { ListItem } from './shared/list-item/list-item.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Test App Sanitas';

  list: ListItem[];

  listSubject: BehaviorSubject<ListItem[]>;
  listObservable: Observable<ListItem[]>;

  ngOnInit() {
    this.list = this.populateList(4000);
    this.listSubject = new BehaviorSubject(this.list);
    this.listObservable = this.listSubject.asObservable();
  }

  updateList(value: string) {
    const filtered = this.list.filter(item => {
      return item.id === value || item.text.includes(value);
    });

    this.listSubject.next(filtered);
  }

  private populateList(numberOfItems: number): ListItem[] {
    const data: ListItem[] = [];

    for (let index = 0; index < numberOfItems; index++) {
      data.push({
        photo: `https://picsum.photos/id/${index}/500/500`,
        text: 'Random text photo number ' + index,
        id: index.toString()
      });
    }

    return data;
  }

}
