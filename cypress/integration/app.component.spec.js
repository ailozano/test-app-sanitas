/// <reference types="cypress" />

context('App component', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  const searchBoxSelector = 'app-search-box input';
  const itemsSelector = 'app-list-item .cdk-virtual-scroll-content-wrapper app-item';
  const itemsSelectorText = itemsSelector + ' .text';

  it('Search box', () => {
    cy.get(searchBoxSelector)
      .type('3000')

    cy.get(itemsSelector)
      .should('have.length', 1);

    cy.get(searchBoxSelector)
      .clear()
      .type('30')

    cy.get(itemsSelector)
      .should('have.length.above', 1);
  });

  it('Virtual scroll update', () => {

    cy.get(itemsSelectorText)
      .then(firstList => {

        const firstListOfText = Array.from(firstList.map((index, el) => {
          return el.textContent;
        }));

        cy.get('app-list-item cdk-virtual-scroll-viewport')
          .scrollTo(0, 5000);

        cy.wait(2000); // Required to see virtual scroll update

        cy.get(itemsSelectorText)
          .should(secondList => {

            const secondListOfText = Array.from(secondList.map((index, item) => {
              return item.textContent;
            }));

            // Compare before and after scroll textContent elements
            const allElementsAreDifferent = firstListOfText
              .map((item, index) => {
                return item !== secondListOfText[index];
              })
              .reduce((accumulator, currentValue) => accumulator && currentValue);

            expect(allElementsAreDifferent).to.be.true;
          });

      });

  });

});
